import { Pipe, PipeTransform } from '@angular/core';
import { TaskState } from '../../models/enums/taskState';

@Pipe({
  name: 'taskstate'
})
export class TaskStatePipe implements PipeTransform {

  transform(value: any, ...args: any[]): string {
    let state: string;

    switch (value as TaskState) {
      case TaskState.ToDo:
        state = 'ToDo'
        break;
      case TaskState.InProgress:
        state = 'InProgress'
        break;
      case TaskState.Canceled:
        state = 'Canceled'
        break;
      case TaskState.Done:
        state = 'Done'
        break;
    }

    return state;
  }
}
