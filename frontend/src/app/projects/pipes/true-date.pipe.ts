import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'truedate'
})
export class TrueDatePipe implements PipeTransform {

  transform(value: Date, ...args: any[]): string {
    let date = new Date(value);
    let month: string; 
    let year: string;
    let day: string;

    year = date.getFullYear().toString()
    day = date.getDate().toString();

    switch (date.getMonth()) {
      case 0:
        month = "января";
        break;
      case 1:
        month = "февраля";
        break;    
      case 2:
        month = "март";
        break;  
      case 3:
        month = "апреля";
        break;
      case 4:
        month = "мая";
        break;    
      case 5:
        month = "июня";
        break;  
      case 6:
        month = "июля";
        break;
      case 7:
        month = "августа";
        break;    
      case 8:
        month = "сентября";
        break;  
      case 9:
        month = "октября";
        break;
      case 10:
        month = "ноября";
        break;    
      case 11:
        month = "декабря";
        break;  
      default:
        month = "января";
        break;        
    }

    return `${day} ${month} ${year}`;
  }
}
