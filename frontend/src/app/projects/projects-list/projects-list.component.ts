import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { IProject } from 'src/app/models/project';
import { ProjectService } from '../project.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from 'src/app/services/snack-bar.service';


@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html'
})
export class ProjectsListComponent implements OnInit, OnDestroy {
  public projects: IProject[] = [];
  public loading: boolean = false;
  
  private unsubscribe$ = new Subject<void>();

  constructor(private projectService: ProjectService,
              private snackBarService: SnackBarService) { }
  
  public ngOnInit(): void {
    this.loading = true;
    this.getProjects();
  }


  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


  getProjects(): void {
    this.projectService.getProjects()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe({
      next: items =>  {
        this.projects = items;
        this.loading = false;
      },
      error: err => {
        this.snackBarService.showErrorMessage(err.error);
        this.loading = false;
      }
    });
  }


  deleteProject(id: number): void {
    if(confirm(`Do you wont to remove this project with ID: ${id}?`)) {
      this.projectService.deleteProject(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: id => {
            this.projects = this.projects.filter(p => p.id !== id);
            this.snackBarService.showUsualMessage(`Project #${id} deleted`);
          },
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
    }
  }
  
}
