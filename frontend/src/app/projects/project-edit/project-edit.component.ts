import { Component, OnInit, OnDestroy } from '@angular/core';
import { IProject } from 'src/app/models/project';
import { ProjectService } from '../project.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { ITeam } from 'src/app/models/team';
import { TeamService } from 'src/app/teams/team.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from 'src/app/services/snack-bar.service';


@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html'
})
export class ProjectEditComponent implements OnInit, OnDestroy {
  public editedProject: IProject = {} as IProject;
  public teams: ITeam[] = [];
  private saved: boolean = false;
  
  private unsubscribe$ = new Subject<void>();

  constructor(private location: Location,
    private projectService: ProjectService,
    private route: ActivatedRoute,
    private teamService: TeamService,
    private snackBarService: SnackBarService) { }


    public ngOnInit(): void {
      const id = Number(this.route.snapshot.paramMap.get('id'));
      this.getProject(id);
      this.getTeams();
    }


    public ngOnDestroy(): void {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
    }

    getTeams(): void {
      this.teamService.getTeams()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: items => this.teams = items,
          error: err => this.snackBarService.showErrorMessage(err)
        });
    }

    getProject(id: number): void {
      this.projectService.getProjectById(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (item) => this.editedProject = item,
        error: err => this.snackBarService.showErrorMessage(err.error)
      });
    }


    editProject(): void  {
      this.projectService.editProject(this.editedProject)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: item => {
            this.snackBarService.showUsualMessage(`Project #${item.id} was edited!`)
            this.goBack();
          },
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
        this.saved = true;
    }


    canDeactivate() : boolean | Observable<boolean>{ 
      if(!this.saved){
          return confirm("Do you want to leave the page with unsaved data?");
      }
      else{
          return true;
      }
    }

    
    goBack(): void {
      this.location.back();
    }
}
