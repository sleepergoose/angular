import { Injectable } from '@angular/core';
import { IProject } from '../models/project';
import { HttpService } from '../services/http.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private readonly routePrefix = 'api/Projects';
  
  constructor(private httpService: HttpService) { }

  
  public getProjectById(id: number): Observable<IProject> {
    return this.httpService.getRequest<IProject>(`${this.routePrefix}/${id}`);
  }


  public getProjects(): Observable<IProject[]> {
    return this.httpService.getRequest<IProject[]>(`${this.routePrefix}`);
  }


  public createProject(project: IProject): Observable<IProject> {
      return this.httpService.postRequest<IProject>(`${this.routePrefix}`, project);
  }


  public editProject(project: IProject): Observable<IProject> {
      return this.httpService.putRequest<IProject>(`${this.routePrefix}`, project);
  }


  public deleteProject(deletedProjectId: number): Observable<number> {
      return this.httpService.deleteRequest<number>(`${this.routePrefix}/${deletedProjectId}`);
  }
}
