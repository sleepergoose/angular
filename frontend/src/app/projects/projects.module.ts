import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ProjectsContainerComponent } from './projects-container/projects-container.component';
import { RouterModule } from '@angular/router';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { TaskStatePipe } from './pipes/task-state.pipe';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { FormsModule } from '@angular/forms';
import { TrueDatePipe } from './pipes/true-date.pipe';
import { MaterialComponentsModule } from '../material-components/material-components.module';

@NgModule({
  declarations: [
    ProjectDetailsComponent,
    ProjectsContainerComponent,
    ProjectsListComponent,
    ProjectEditComponent,
    TaskStatePipe,
    TrueDatePipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MaterialComponentsModule
  ],
  exports: [
    ProjectDetailsComponent,
    ProjectsContainerComponent,
    ProjectEditComponent,
    ProjectsListComponent,
    TrueDatePipe,
    TaskStatePipe
  ]
})
export class ProjectsModule { }
