import { Component, OnInit, OnDestroy } from '@angular/core';
import { IProject } from 'src/app/models/project';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from '../project.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from 'src/app/services/snack-bar.service';


@Component({
  selector: 'project-item',
  templateUrl: './project-details.component.html'
})
export class ProjectDetailsComponent implements OnInit, OnDestroy {
  public project = {} as IProject;

  private unsubscribe$ = new Subject<void>();

  constructor(private location: Location,
              private projectService: ProjectService,
              private route: ActivatedRoute,
              private snackBarService: SnackBarService) { }


  public ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.getProject(id);
  }


  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  
  getProject(id: number): void {
    this.projectService.getProjectById(id)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe({
      next: (item) => this.project = item,
      error: err => this.snackBarService.showErrorMessage(err.error)
    });
  }

  
  deleteProject(id: number): void {
    if(confirm(`Do you wont to remove this project with ID: ${id}?`)) {
      this.projectService.deleteProject(id)
      .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: (id) => { 
            this.snackBarService.showErrorMessage(`Projec with Id ${id} deleted!`);
            this.goBack();
          },
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
    }
  }


  goBack(): void {
    this.location.back();
  }
}
