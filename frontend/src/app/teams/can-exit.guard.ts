import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TeamEditComponent } from './team-edit/team-edit.component';

@Injectable({
  providedIn: 'root'
})
export class TeamCanExitGuard implements CanDeactivate<TeamEditComponent> {
  canDeactivate(
    component: TeamEditComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      return component.canDeactivate();
  }
  
}
