import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ITeam } from 'src/app/models/team';
import { TeamService } from '../team.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html'
})
export class TeamDetailsComponent implements OnInit, OnDestroy {
  public team = {} as ITeam;

  private unsubscribe$ = new Subject<void>();

  constructor(private location: Location,
              private teamService: TeamService,
              private route: ActivatedRoute,
              private snackBarService: SnackBarService) { }


  public ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.getTeam(id);
  }


  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  

  getTeam(id: number): void {
    this.teamService.getTeamById(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: item => this.team = item,
        error: err => this.snackBarService.showErrorMessage(err.error)
      });
  }


  deleteTeam(id: number): void {
    if(confirm(`Do you wont to remove this team with ID: ${id}?`)) {
      this.teamService.deleteTeam(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: id => {
            this.snackBarService.showUsualMessage(`Team #${id} deleted`);
            this.goBack();
          },
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
    }
  }


  goBack(): void {
    this.location.back();
  }
}
