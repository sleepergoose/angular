import { Component, OnInit, OnDestroy } from '@angular/core';
import { ITeam } from 'src/app/models/team';
import { TeamService } from '../team.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';


@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html'
})
export class TeamsListComponent implements OnInit, OnDestroy {
  public teams: ITeam[] = [];
  public loading: boolean = false;

  private unsubscribe$ = new Subject<void>();

  constructor(private teamService: TeamService,
              private snackBarService: SnackBarService) { }

  public ngOnInit(): void {
    this.loading = true;
    this.getTeams();
  }


  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


  getTeams(): void {
    this.teamService.getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: items => {
          this.teams = items;
          this.loading = false;
        },
        error: err => {
          this.snackBarService.showErrorMessage(err.error);
          this.loading = false;
        }
      });
  }


  deleteTeam(id: number): void {
    if(confirm(`Do you wont to remove this team with ID: ${id}?`)) {
      this.teamService.deleteTeam(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: id =>{
            this.snackBarService.showUsualMessage(`Team #${id} deleted`);
            this.teams = this.teams.filter(t => t.id !== id);
          },
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
    }
  }
}