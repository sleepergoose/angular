import { Injectable } from '@angular/core';
import { ITeam } from '../models/team';
import { HttpService } from '../services/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private readonly routePrefix = 'api/Teams';
  
  constructor(private httpService: HttpService) { }

  
  public getTeamById(id: number): Observable<ITeam> {
    return this.httpService.getRequest<ITeam>(`${this.routePrefix}/${id}`);
  }


  public getTeams(): Observable<ITeam[]> {
    return this.httpService.getRequest<ITeam[]>(`${this.routePrefix}`);
  }


  public createTeam(project: ITeam): Observable<ITeam> {
      return this.httpService.postRequest<ITeam>(`${this.routePrefix}`, project);
  }


  public editTeam(project: ITeam): Observable<ITeam> {
      return this.httpService.putRequest<ITeam>(`${this.routePrefix}`, project);
  }


  public deleteTeam(deletedProjectId: number): Observable<number> {
      return this.httpService.deleteRequest<number>(`${this.routePrefix}/${deletedProjectId}`);
  }
}
