import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TeamsContainerComponent } from './teams-container/teams-container.component';
import { TeamsListComponent } from './teams-list/teams-list.component';
import { ProjectsModule } from '../projects/projects.module';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { TeamEditComponent } from './team-edit/team-edit.component';
import { FormsModule } from '@angular/forms';
import { MaterialComponentsModule } from '../material-components/material-components.module';

@NgModule({
  declarations: [
    TeamsContainerComponent,
    TeamsListComponent,
    TeamDetailsComponent,
    TeamEditComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ProjectsModule,
    FormsModule,
    MaterialComponentsModule
  ]
})
export class TeamsModule { }
