import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { ITeam } from 'src/app/models/team';
import { TeamService } from '../team.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from 'src/app/services/snack-bar.service';


@Component({
  selector: 'app-team-edit',
  templateUrl: './team-edit.component.html'
})
export class TeamEditComponent implements OnInit, OnDestroy {
  public editedTeam: ITeam = {} as ITeam;
  private saved: boolean = false;


  constructor(private location: Location,
    private teamService: TeamService,
    private route: ActivatedRoute,
    private snackBarService: SnackBarService) { }

    private unsubscribe$ = new Subject<void>();

    public ngOnInit(): void {
      const id = Number(this.route.snapshot.paramMap.get('id'));
      this.getTeam(id);
    }


    public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
    }


    getTeam(id: number): void {
      this.teamService.getTeamById(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: item => this.editedTeam = item,
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
    }


    editTeam(): void  {
      this.teamService.editTeam(this.editedTeam)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: item => {
            this.snackBarService.showUsualMessage(`Team #${item.id} edited`);
            this.goBack();
          },
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
        this.saved = true;
    }


    canDeactivate() : boolean | Observable<boolean> {
      if(!this.saved){
          return confirm("Do you want to leave the page with unsaved data?");
      }
      else{
          return true;
      }
    }

    
    goBack(): void {
      this.location.back();
    }
}
