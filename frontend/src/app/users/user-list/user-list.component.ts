import { Component, OnInit, OnDestroy } from '@angular/core';
import { IUser } from 'src/app/models/user';
import { UserService } from '../user.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit, OnDestroy {
  public users: IUser[] = [];
  public loading: boolean = false;

  private unsubscribe$ = new Subject<void>();

  constructor(private userService: UserService,
              private snackBarService: SnackBarService) { }

  public ngOnInit(): void {
    this.loading = true;
    this.getUsers();
  }
  

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


  getUsers(): void {
    this.userService.getUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: items => {
          this.users = items;
          this.loading = false;
        },
        error: err => {
          this.loading = false;
          this.snackBarService.showErrorMessage(err.error);
        }
      });
  }


  deleteUser(id: number): void {
    if(confirm(`Do you wont to remove this user with ID: ${id}?`)) {
      this.userService.deleteUser(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: id => {
            this.snackBarService.showUsualMessage(`User #${id as number} was deleted`);
            this.users = this.users.filter(u => u.id !== id);
          },
          error: err => this.snackBarService.showErrorMessage("It may not be possible to delete this user due to database limitations" + err.error)
        });
    }
  }
}
