import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProjectsModule } from '../projects/projects.module';
import { FormsModule } from '@angular/forms';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserContainerComponent } from './user-container/user-container.component';
import { UserListComponent } from './user-list/user-list.component';
import { MaterialComponentsModule } from '../material-components/material-components.module';

@NgModule({
  declarations: [
    UserEditComponent,
    UserContainerComponent,
    UserListComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ProjectsModule,
    FormsModule,
    MaterialComponentsModule
  ]
})
export class UsersModule { }
