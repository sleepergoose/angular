import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from '../models/user';
import { HttpService } from '../services/http.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly routePrefix = 'api/Users';
  
  constructor(private httpService: HttpService) { }

  
  public getUserById(id: number): Observable<IUser> {
    return this.httpService.getRequest<IUser>(`${this.routePrefix}/${id}`);
  }


  public getUsers(): Observable<IUser[]> {
    return this.httpService.getRequest<IUser[]>(`${this.routePrefix}`);
  }


  public createUser(project: IUser): Observable<IUser> {
      return this.httpService.postRequest<IUser>(`${this.routePrefix}`, project);
  }


  public editUser(project: IUser): Observable<IUser> {
      return this.httpService.putRequest<IUser>(`${this.routePrefix}`, project);
  }


  public deleteUser(deletedProjectId: number): Observable<number> {
      return this.httpService.deleteRequest<number>(`${this.routePrefix}/${deletedProjectId}`);
  }
}
