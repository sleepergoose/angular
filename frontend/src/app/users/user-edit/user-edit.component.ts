import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { IUser } from 'src/app/models/user';
import { UserService } from '../user.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html'
})
export class UserEditComponent implements OnInit, OnDestroy {
  public editedUser: IUser = {} as IUser;
  private saved: boolean = false;


  constructor(private location: Location,
    private userService: UserService,
    private route: ActivatedRoute,
    private snackBarService: SnackBarService) { }

    private unsubscribe$ = new Subject<void>();

    public ngOnInit(): void {
      const id = Number(this.route.snapshot.paramMap.get('id'));
      this.getUser(id);
    }

    public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
    }


    getUser(id: number): void {
      this.userService.getUserById(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: item => this.editedUser = item,
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
    }


    editUser(): void  {
      this.userService.editUser(this.editedUser)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: item => {
            this.snackBarService.showUsualMessage(`User #${item.id} edited`);
            this.goBack();
          },
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
        this.saved = true;
    }


    canDeactivate() : boolean | Observable<boolean> {
      if(!this.saved){
          return confirm("Do you want to leave the page with unsaved data?");
      }
      else{
          return true;
      }
    }

    goBack(): void {
      this.location.back();
    }
}
