import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectDetailsComponent } from './projects/project-details/project-details.component';
import { ProjectsListComponent } from './projects/projects-list/projects-list.component';
import { ProjectEditComponent } from './projects/project-edit/project-edit.component';
import { ProjectExitGuard } from './projects/project-exit.guard';
import { TeamsListComponent } from './teams/teams-list/teams-list.component';
import { TeamDetailsComponent } from './teams/team-details/team-details.component';
import { TeamEditComponent } from './teams/team-edit/team-edit.component';
import { TeamCanExitGuard } from './teams/can-exit.guard';
import { UserListComponent } from './users/user-list/user-list.component';
import { UserEditComponent } from './users/user-edit/user-edit.component';
import { UserCanExitGuard } from './users/user-can-exit.guard';
import { TasksListComponent } from './tasks/tasks-list/tasks-list.component';
import { TaskDetailsComponent } from './tasks/task-details/task-details.component';
import { TaskEditComponent } from './tasks/task-edit/task-edit.component';
import { TaskCanExitGuard } from './tasks/task-can-exit.guard';
import { ProjectsContainerComponent } from './projects/projects-container/projects-container.component';
import { TeamsContainerComponent } from './teams/teams-container/teams-container.component';
import { UserContainerComponent } from './users/user-container/user-container.component';
import { TasksContainerComponent } from './tasks/tasks-container/tasks-container.component';


const routes: Routes = [
  { path: '', component: ProjectsListComponent},
  
  { path: 'projects', component: ProjectsContainerComponent,
      children: [ { path: '', component: ProjectsListComponent },
                  { path: ':id', component: ProjectDetailsComponent },
                  { path: ':id/edit', component: ProjectEditComponent, canDeactivate: [ProjectExitGuard]} ]},

  { path: 'teams', component: TeamsContainerComponent,
      children: [ { path: '', component: TeamsListComponent },
                  { path: ':id', component: TeamDetailsComponent },
                  { path: ':id/edit', component: TeamEditComponent, canDeactivate: [TeamCanExitGuard]} ]},

  { path: 'users', component: UserContainerComponent,
      children: [ { path: '', component: UserListComponent },
                  { path: ':id/edit', component: UserEditComponent, canDeactivate: [UserCanExitGuard]} ]},

  { path: 'tasks', component: TasksContainerComponent,
      children: [ { path: '', component: TasksListComponent },
                  { path: ':id', component: TaskDetailsComponent },
                  { path: ':id/edit', component: TaskEditComponent, canDeactivate: [TaskCanExitGuard]} ]},
                  
  { path: '**', component: ProjectsListComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
