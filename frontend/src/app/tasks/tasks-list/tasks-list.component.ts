import { Component, OnInit, OnDestroy } from '@angular/core';
import { ITask } from 'src/app/models/task';
import { TaskService } from '../task.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html'
})
export class TasksListComponent implements OnInit, OnDestroy {
  public tasks: ITask[] = [];
  public loading: boolean = false;

  private unsubscribe$ = new Subject<void>();

  constructor(private taskService: TaskService,
              private snackBarService: SnackBarService) { }

  
  ngOnInit(): void {
    this.loading = true;
    this.getTasks();
  }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


  getTasks(): void {
    this.taskService.getTasks()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: items => { 
          this.tasks = items;
          this.loading = false;
        },
        error: err => {
          this.loading = false;
          this.snackBarService.showErrorMessage(err.error);
        }
      });
  }


  deleteTask(id: number): void {
    if(confirm(`Do you wont to remove this task with ID: ${id}?`)) {
      this.taskService.deleteTask(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: id => {
            this.tasks = this.tasks.filter(t => t.id !== id);
            this.snackBarService.showUsualMessage(`Task #${id} deleted`);
        },
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
    }
  }
}
