import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ITask } from 'src/app/models/task';
import { TaskService } from '../task.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';


@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html'
})
export class TaskDetailsComponent implements OnInit, OnDestroy {
  public task: ITask[] = [] // {} as ITask;

  private unsubscribe$ = new Subject<void>();

  constructor(private location: Location,
              private taskService: TaskService,
              private route: ActivatedRoute,
              private snackBarService: SnackBarService) { }


  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.getTask(id);
  }


  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();    
  }


  getTask(id: number): void {
    this.taskService.getTaskById(id)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe({
      next: (item) => this.task.push(item),
      error: err => this.snackBarService.showErrorMessage(err.error)
    });
  }

  
  deleteTask(id: number): void {
    if(confirm(`Do you wont to remove this task with ID: ${id}?`)) {
      this.taskService.deleteTask(id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: id => { 
            this.goBack();
            this.snackBarService.showUsualMessage(`Task #${id} deleted`);
          },
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
    }
  }


  goBack(): void {
    this.location.back();
  }
}
