import { Injectable } from '@angular/core';
import { HttpService } from '../services/http.service';
import { Observable } from 'rxjs';
import { ITask } from '../models/task';


@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private readonly routePrefix = 'api/Tasks';
  
  constructor(private httpService: HttpService) { }

  
  public getTaskById(id: number): Observable<ITask> {
    return this.httpService.getRequest<ITask>(`${this.routePrefix}/${id}`);
  }


  public getTasks(): Observable<ITask[]> {
    return this.httpService.getRequest<ITask[]>(`${this.routePrefix}`);
  }


  public createTask(project: ITask): Observable<ITask> {
      return this.httpService.postRequest<ITask>(`${this.routePrefix}`, project);
  }


  public editTask(project: ITask): Observable<ITask> {
      return this.httpService.putRequest<ITask>(`${this.routePrefix}`, project);
  }


  public deleteTask(deletedProjectId: number): Observable<number> {
      return this.httpService.deleteRequest<number>(`${this.routePrefix}/${deletedProjectId}`);
  }
}
