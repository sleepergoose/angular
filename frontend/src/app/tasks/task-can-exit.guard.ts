import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TaskEditComponent } from './task-edit/task-edit.component';

@Injectable({
  providedIn: 'root'
})
export class TaskCanExitGuard implements CanDeactivate<TaskEditComponent> {
  canDeactivate(
    component: TaskEditComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean> | boolean {
    
      return component.canDeactivate();
  }
  
}
