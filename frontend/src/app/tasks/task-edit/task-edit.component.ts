import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ITask } from 'src/app/models/task';
import { TaskService } from '../task.service';
import { TaskState } from 'src/app/models/enums/taskState';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';


@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html'
})
export class TaskEditComponent implements OnInit, OnDestroy {
  public editedTask: ITask = {} as ITask;
  private saved: boolean = false;
  
  public selectControlValues: TaskState[] = [ TaskState.ToDo, TaskState.InProgress, TaskState.Canceled, TaskState.Done];

  constructor(private location: Location,
    private taskService: TaskService,
    private route: ActivatedRoute,
    private snackBarService: SnackBarService) { }

    private unsubscribe$ = new Subject<void>();

    ngOnInit(): void {
      const id = Number(this.route.snapshot.paramMap.get('id'));
      this.getTask(id);
    }


    public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
    }


    getTask(id: number): void {
      this.taskService.getTaskById(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (item) => this.editedTask = item,
        error: err => this.snackBarService.showErrorMessage(err.error)
      });
    }

    
    editTask(): void  {
      this.taskService.editTask(this.editedTask)
      .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: item => {
            this.goBack();
            this.snackBarService.showUsualMessage(`Task #${item.id} edited`);
          },
          error: err => this.snackBarService.showErrorMessage(err.error)
        });
        this.saved = true;
    }


    canDeactivate() : boolean | Observable<boolean> {
      if(!this.saved){
          return confirm("Do you want to leave the page with unsaved data?");
      }
      else{
          return true;
      }
    }

    
    goBack(): void {
      this.location.back();
    }
}
