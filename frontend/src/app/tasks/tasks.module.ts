import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TasksContainerComponent } from './tasks-container/tasks-container.component';
import { TaskEditComponent } from './task-edit/task-edit.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { RouterModule } from '@angular/router';
import { ProjectsModule } from '../projects/projects.module';
import { FormsModule } from '@angular/forms';
import { CanceledTaskDirective } from './directives/canceled-task.directive';
import { MaterialComponentsModule } from '../material-components/material-components.module';


@NgModule({
  declarations: [
    TasksListComponent,
    TasksContainerComponent,
    TaskEditComponent,
    TaskDetailsComponent,
    CanceledTaskDirective
  ],
  imports: [
    CommonModule,
    RouterModule,
    ProjectsModule,
    FormsModule,
    MaterialComponentsModule
  ]
})
export class TasksModule { }
