import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[taskStateDirective]'
})
export class CanceledTaskDirective implements OnInit  {
  @Input() option: string = '';
  

  constructor(private elementRef: ElementRef) { }

  ngOnInit(): void {
    switch (this.option) {
      case '0': // ToDo
        this.elementRef.nativeElement.style.backgroundColor = '#eef00750';
        break;
       case '1': // InProgress
        this.elementRef.nativeElement.style.backgroundColor = '#0ba5e350';
        break;   
       case '2': // Done
        this.elementRef.nativeElement.style.backgroundColor = '#1ca43e50';
        break;  
       case '3': // Canceled
        this.elementRef.nativeElement.style.backgroundColor = '#ff7d7d50';
        break;                  
    }
  }
  
}
