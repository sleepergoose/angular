import { IUser } from './user';

export interface ITeam {
    id: number, 
    name: string,
    createdAt: Date
    members: IUser[]
}