import { IUser } from './user';

export interface ITask {
    id: number,
    projectId: number,
    performer: IUser,
    name: string,
    description: string,
    state: TaskState,
    createdAt: Date,
    finishedAt?: Date
}