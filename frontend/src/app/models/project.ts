import { IUser } from './user';
import { ITeam } from './team';
import { ITask } from './task';

export interface IProject {
    id: number;
    author: IUser,
    team: ITeam,
    name: string,
    description: string,
    deadline: Date,
    createdAt: Date,
    tasks: ITask[]
}