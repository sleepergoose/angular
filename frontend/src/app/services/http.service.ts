import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private baseUrl: string = environment.apiUrl;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  
  public getRequest<T>(endpoint: string, httpParams?: HttpParams): Observable<T> {
    return this.http.get<T>(this.getUrl(endpoint));
  }


  public postRequest<T>(endpoint: string, payload: object): Observable<T> {
    return this.http.post<T>(this.getUrl(endpoint), payload, this.httpOptions);
  }


  public putRequest<T>(endpoint: string, payload: object): Observable<T> {
    return this.http.put<T>(this.getUrl(endpoint), payload, this.httpOptions);
  }


  public deleteRequest<T>(endpoint: string, httpParams?: HttpParams): Observable<T> {
    return this.http.delete<T>(this.getUrl(endpoint), { params: httpParams });
  }


  private getUrl(endpoint: string): string {
    return `${this.baseUrl}/${endpoint}`;
  }
}
