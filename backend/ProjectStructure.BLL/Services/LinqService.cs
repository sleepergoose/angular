﻿using System;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class LinqService : BaseService
    {
        public LinqService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }



        /* -- 1 -- */
        public async Task<Dictionary<ProjectDTO, int>> GetTasksAmountAsync(int authorId)
        {
            var result = await _context.Projects.Where(project => project.AuthorId == authorId)
                .Include(p => p.Tasks)
                .AsNoTracking()
                .ToDictionaryAsync(key => key, value => (value.Tasks == null ? 0 : value.Tasks.Count()));

            return _mapper.Map<Dictionary<ProjectDTO, int>>(result);
        }



        /* -- 2 -- */
        public async Task<List<TaskDTO>> GetTasksListAsync(int performerId)
        {
            var result = await _context.Tasks.Where(task => task.PerformerId == performerId
                                        && task.Name.Length < 45).AsNoTracking().ToListAsync();
            
            return _mapper.Map<List<TaskDTO>>(result);
        }



        /* -- 3 -- */
        public async Task<List<(int Id, string Name)>> GetFinishedTasksAsync(int performerId)
        {
            return await Task.Run(() => _context.Tasks.Where(task =>
                                                task.PerformerId == performerId &&
                                                task.FinishedAt != null &&
                                                task.FinishedAt.Value.Year == DateTime.Now.Year)
                                        .AsNoTracking().AsEnumerable()
                                        .Select(task => (id: task.Id, name: task.Name))
                                        .ToList());
        }


        /* -- 4 -- */
        public async Task<List<(int Id, string TeamName, List<UserDTO> Users)>> GetTeamsMembersAsync()
        {
            return await Task.Run(() => _context.Teams
                    .Include(team => team.Members)
                    .Where(team => team.Members.All(m => m.BirthDay.Year < DateTime.Now.Year - 10))
                    .AsNoTracking()
                    .AsEnumerable()
                    .Select(team => (
                           Id: team.Id,
                           TeamName: team.Name,
                           Users: _mapper.Map<List<UserDTO>>(team.Members
                                        .OrderByDescending(performer => performer.RegisteredAt).ToList())
                    )).ToList());
        }



        /* -- 5 -- */
        public async Task<List<UserWithTasksDTO>> GetUsersWithTasksAsync()
        {
            return await Task.Run(() => _context.Users
                    .Include(user => user.Tasks)
                    .OrderBy(user => user.FirstName)
                    .AsNoTracking()
                    .AsEnumerable()
                    .Select(user => new UserWithTasksDTO
                    {
                        User = _mapper.Map<UserDTO>(user),
                        Tasks = _mapper.Map<List<TaskDTO>>(user.Tasks.OrderByDescending(t => t.Name.Length).ToList())
                    })
                    .ToList());
        }


        /* -- 6 -- */
        public async Task<UserSummaryDTO> GetUserSummaryAsync(int userId)
        {
            // Separate queries because it can be that user has no project, but he has tasks
            var author = await Task.Run(() => _context.Users.Where(u => u.Id == userId)
                .Include(u => u.Tasks)
                .AsNoTracking()
                .FirstOrDefaultAsync());


            if (author == null)
                return null;


            var project = await Task.Run(() => _context.Projects
                .Where(p => p.AuthorId == userId)
                .OrderByDescending(p => p.CreatedAt)
                .Take(1)
                .Include(p => p.Tasks)
                .AsNoTracking()
                .FirstOrDefaultAsync());


            return new UserSummaryDTO()
            {
                User = _mapper.Map<UserDTO>(author),

                LastProject = _mapper.Map<ProjectDTO>(project),

                LastProjectTasksAmount = project?.Tasks?.Count() ?? 0,

                BadTasksAmount = author.Tasks == null ? 0
                    : author.Tasks.Where(task => task.FinishedAt == null).Count(),

                LongestTask = _mapper.Map<TaskDTO>(author.Tasks?
                    .OrderByDescending(t => (t.FinishedAt == null ? DateTime.Now : t.FinishedAt) - t.CreatedAt)
                    .FirstOrDefault())
            };
        }


        /* -- 7 -- */
        public async Task<List<ProjectSummaryDTO>> GetProjectSummaryAsync()
        {
            return await Task.Run(() =>
                _context.Projects
                    .Include(p => p.Tasks)
                    .Include(p => p.Team)
                        .ThenInclude(team => team.Members)
                    .AsNoTracking()
                    .AsEnumerable()
                    .Select(project => new ProjectSummaryDTO
                    {

                        Project = _mapper.Map<ProjectDTO>(project),

                        LongestTaskByDescription = _mapper.Map<TaskDTO>(project.Tasks?.OrderByDescending(task => task.Description.Length)
                                                        .FirstOrDefault()),

                        ShortestTaskByName = _mapper.Map<TaskDTO>(project.Tasks?.OrderBy(task => task.Name.Length).FirstOrDefault()),

                        TeamMemberAmount = new Func<int>(() => {
                            if (project.Description.Length > 20 || (project.Tasks != null && project.Tasks.Count() < 3))
                            {
                                return project.Team == null ? 0 : project.Team.Members.Count();
                            }
                            return 0;
                        }).Invoke() /* complex ternary operator - it's bad */

                    }).ToList()
                );
        }
    }
}