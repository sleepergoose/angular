﻿using System;
using AutoMapper;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.BLL.Services.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public sealed class TaskService : BaseService, IService<TaskDTO>
    {
        public TaskService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        public async Task<IEnumerable<TaskDTO>> GetAllEntitiesAsync()
        {
            var tasks = await _context.Tasks
                .Include(task => task.Performer)
                .AsNoTracking()
                .ToListAsync();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }


        public async Task<TaskDTO> GetEntityAsync(int id)
        {
            var task = await _context.Tasks
                .Include(task => task.Performer)
                .AsNoTracking()
                .FirstOrDefaultAsync(p => p.Id == id);

            return _mapper.Map<TaskDTO>(task);
        }


        public async Task<TaskDTO> AddEntityAsync(TaskDTO taskDTO)
        {
            if (taskDTO == null)
                throw new ArgumentNullException("Argument cannot be null");

            try
            {
                taskDTO.Id = 0;

                var task = _mapper.Map<DAL.Entities.Task>(taskDTO);

                await _context.Tasks.AddAsync(task);

                await _context.SaveChangesAsync();

                var createdTask = await GetEntityAsync(task.Id);

                return _mapper.Map<TaskDTO>(createdTask);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<TaskDTO> UpdateEntityAsync(TaskDTO taskDTO)
        {
            if (taskDTO == null)
                throw new ArgumentNullException("Argument cannot be null");

            try
            {
                var task = _mapper.Map<DAL.Entities.Task>(taskDTO);

                _context.Entry(task).State = EntityState.Modified;

                await _context.SaveChangesAsync();

                var updatedTask = await GetEntityAsync(task.Id);

                return _mapper.Map<TaskDTO>(updatedTask);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<int> DeleteEntityAsync(int id)
        {
            try
            {
                var deletedTask = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == id);

                _context.Tasks.Remove(deletedTask);

                await _context.SaveChangesAsync();

                return id;
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentNullException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
