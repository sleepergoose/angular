﻿using System;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public class UnfinishedTaskService : BaseService
    {
        public UnfinishedTaskService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        public async Task<List<TaskDTO>> GetUnfinishedTasksAsync(int performerId)
        {
            if (performerId <= 0)
                throw new ArgumentOutOfRangeException("Performer ID cannot be less than or equal to zero");

            try
            {
                var unfinishedTasks = await _context.Tasks.AsNoTracking()
                    .Where(task => task.PerformerId == performerId && task.FinishedAt == null).ToListAsync();

                return _mapper.Map<List<TaskDTO>>(unfinishedTasks);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
