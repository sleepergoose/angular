﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BLL.DTO
{
    public sealed class ProjectDTO
    {
        public int Id { get; set; }

        [Required]
        public UserDTO Author { get; set; }

        public TeamDTO Team { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        [MinLength(3)]
        public string Description { get; set; }

        [Required]
        public DateTime Deadline { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        public ICollection<TaskDTO> Tasks { get; set; }
    }
}

