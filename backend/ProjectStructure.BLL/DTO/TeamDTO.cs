﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BLL.DTO
{
    public sealed class TeamDTO
    {
        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }


        [Required]
        public DateTime CreatedAt { get; set; }

        public ICollection<UserDTO> Members { get; set; }
    }
}
