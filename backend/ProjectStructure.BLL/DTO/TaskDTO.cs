﻿using System;
using ProjectStructure.DAL.Enums;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BLL.DTO
{
    public sealed class TaskDTO
    {
        public int Id { get; set; }

        [Required]
        public int ProjectId { get; set; }

        [Required]
        public UserDTO Performer { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        [MinLength(3)]
        public string Description { get; set; }

        [Required]
        public TaskState State { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        public DateTime? FinishedAt { get; set; }
    }
}
