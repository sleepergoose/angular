﻿using ProjectStructure.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProjectStructure.DAL.Context.EntityTypeConfigurations
{
    public sealed class TaskConfiguration : IEntityTypeConfiguration<Task>
    {
        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.Property(p => p.Name)
               .IsRequired()
               .HasColumnType("nvarchar(128)")
               .HasColumnName("TaskName");


            builder.Property(p => p.Description)
                .IsRequired(false)
                .HasColumnType("nvarchar(512)");


            builder.Property(p => p.State)
                .IsRequired()
                .HasColumnType("int");


            builder.Property(p => p.CreatedAt)
                .IsRequired()
                .HasColumnType("datetime2(2)")
                .HasDefaultValueSql("GETDATE()");


            builder.Property(p => p.FinishedAt)
                .IsRequired(false)
                .HasColumnType("datetime2(2)");


            builder.HasOne(t => t.Project)
                .WithMany(p => p.Tasks)
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);


            builder.HasOne(t => t.Performer)
                .WithMany(u => u.Tasks)
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
