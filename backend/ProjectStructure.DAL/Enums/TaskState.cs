﻿
namespace ProjectStructure.DAL.Enums
{
    public enum TaskState
    {
        ToDo = 0,
        InProgress,
        Done,
        Canceled
    }
}
