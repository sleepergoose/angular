﻿using System.Reflection;
using ProjectStructure.BLL.DTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.MappingProfiles;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.BLL.Services.Interfaces;

namespace ProjectStructure.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<ProjectProfile>();
            },
            Assembly.GetExecutingAssembly());
        }


        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddScoped<IService<ProjectDTO>, ProjectService>();
            services.AddScoped<IService<UserDTO>, UserService>();
            services.AddScoped<IService<TeamDTO>, TeamService>();
            services.AddScoped<IService<TaskDTO>, TaskService>();

            services.AddScoped<LinqService>();
            services.AddScoped<UnfinishedTaskService>();
        }
    }
}
