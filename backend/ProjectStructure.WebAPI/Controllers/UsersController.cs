﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using ProjectStructure.BLL.Services.Interfaces;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IService<UserDTO> _userService;


        public UsersController(IService<UserDTO> userService)
        {
            _userService = userService;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> Get()
        {
            return Ok(await _userService.GetAllEntitiesAsync());
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            if (id <= 0)
                return BadRequest("User ID cannot be less than or equal to zero");

            var user = await _userService.GetEntityAsync(id);

            if (user == null)
                return NotFound("There are no user with this Id");

            return Ok(user);
        }


        [HttpPost]
        public async Task<ActionResult> PostAsync([FromBody] UserDTO userDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var user = await _userService.AddEntityAsync(userDTO);
                return CreatedAtAction(nameof(Get), new { Id = user.Id }, user);
            }
            catch (ArgumentException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpPut]
        public async Task<ActionResult> Put([FromBody] UserDTO userDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var user = await _userService.UpdateEntityAsync(userDTO);
                return Ok(user);
            }
            catch (ArgumentException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (id <= 0)
                return BadRequest("User ID cannot be less than or equal to zero");

            try
            {
                var user = await _userService.GetEntityAsync(id);

                if (user == null)
                    return NotFound("There are no user with this Id");

                return Ok(await _userService.DeleteEntityAsync(id));
            }
            catch (ArgumentNullException ex)
            {
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
